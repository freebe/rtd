﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Startup : MonoBehaviour {

    //private AnotherScript anotherScript;    
    public int user=2;
    public GameObject Player;
    private GameObject[] Playerlist;

    // Use this for initialization
    void Start () {
        user = Main.tmpP;
        Vector3 init = new Vector3(2, 0.5f, -13.5f);
        Playerlist = new GameObject[user];

        //Animator animator1 = gameObject.GetComponent<Animator>();
        //animator1.runtimeAnimatorController = Resources.Load("Horse/Model/Horse Animator Controller") as RuntimeAnimatorController;

        for (int i=0; i<user; i++)
        {
            Player = Resources.Load("Horse/Model/Horse") as GameObject;
            Instantiate(Player).transform.Translate(init);
            init = init - new Vector3(2, 0, 0);
            Playerlist[i] = Player;
        }
    }
	
	// Update is called once per frame

	void Update () {
        //Playerlist[0].AddComponent(animator1);
        Debug.Log("update finish");
    }
}
