﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Main : MonoBehaviour {

    static public int tmpP=1;
    GameObject up;
    
    // Use this for initialization
    void Start () {
        up = GameObject.Find("Player");
        tmpP = int.Parse(up.GetComponent<Text>().text);
        
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void click_up()
    {
        if(tmpP<15) tmpP++;
        up.GetComponent<Text>().text = tmpP.ToString();
    }

    public void click_down()
    {
        if (tmpP > 2) tmpP--;
        up.GetComponent<Text>().text = tmpP.ToString();
    }

    public void Play()
    {
        SceneManager.LoadScene(2);
        //SceneManager.LoadScene(1);
    }

}
