﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class Go : MonoBehaviour {

    GameObject text;

    // Use this for initialization
    void Start () {
        text = GameObject.Find("Play");
        StartCoroutine(BlinkText());
    }

    private IEnumerator BlinkText()
    {
        while (true) {
            GetComponent<Text>().color = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            text.GetComponent<Text>().text= "";
            yield return new WaitForSeconds(.5f);
            text.GetComponent<Text>().text = "Play";
            yield return new WaitForSeconds(.5f);
        }
    }

    // Update is called once per frame
    void Update () {
        //text.GetComponent<Text>().color = ;
    }

}
