﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {
    GameObject Camera1;
    GameObject Camera2;

    void Start()
    {
        Camera1 = GameObject.Find("Camera1");
        Camera2 = GameObject.Find("Camera2");

        Camera1.GetComponent<Camera>().enabled = true;
        Camera2.GetComponent<Camera>().enabled = false;
    }

    void Update()
    {
        if (Input.GetKey("1"))
        {
            Camera1.GetComponent<Camera>().enabled = false;
            Camera2.GetComponent<Camera>().enabled = true;
        }
        else if (Input.GetKey("2"))
        {
            Camera1.GetComponent<Camera>().enabled = true;
            Camera2.GetComponent<Camera>().enabled = false;
        }
        else { }
    }
}
